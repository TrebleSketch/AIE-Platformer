﻿using UnityEngine;
using System.Collections.Generic;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class Platform : MonoBehaviour
{
    public enum MovementType
    {
        Stationary,
        BreakOnceLanded,
        SideToSide,
        UpDownUp,
        AroundTheEdge // travelling around the edge of the collider, using waypoints but only linearlly
    }

    public bool startImmediately;
    bool activated;

    public MovementType platformerMovement;

    // todo: Enum pop ups

    // Stationary Platforms

    // Breaking Platforms

    // Moving Platforms (Side to Side, Around the Edge)
    readonly List<Vector3> waypoints = new List<Vector3>();
    Vector3 velocity;
    const float smoothTime = 0.5f;
    const float maxSpeed = 1f;
    bool once;
    Vector3 newPosition;

    public Vector2 movementDirection;

    void Start()
    {
        waypoints.Add(transform.localPosition);

        switch (platformerMovement)
        {
            case MovementType.Stationary:
                break;
            case MovementType.BreakOnceLanded:
                break;
            case MovementType.SideToSide:
            case MovementType.UpDownUp:
                LinearMovementStart(platformerMovement);
                break;
            case MovementType.AroundTheEdge:
                break;
            default:
                break;
        }

        //print(waypoints[0] + " ==> " + waypoints[1]);

        //print(waypoints[0] + " ==> " + new Vector3(waypoints[1].x - 0.1f, waypoints[1].y, waypoints[1].z));

        activated = startImmediately;
    }
    
    void Update()
    {
        if (!activated)
            return;

        switch (platformerMovement)
        {
            case MovementType.Stationary:
                break;
            case MovementType.BreakOnceLanded:
                break;
            case MovementType.SideToSide:
            case MovementType.UpDownUp:
                LinearMovementUpdate();
                break;
            case MovementType.AroundTheEdge:
                break;
            default:
                break;
        }
    }

    void LinearMovementStart(MovementType type)
    {
        switch (type)
        {
            case MovementType.SideToSide:
                waypoints.Add(new Vector3(transform.localPosition.x + movementDirection.x, transform.localPosition.y, transform.localPosition.z));
                break;
            case MovementType.UpDownUp:
                waypoints.Add(new Vector3(transform.localPosition.x, transform.localPosition.y + movementDirection.y, transform.localPosition.z));
                break;
        }
    }

    void LinearMovementUpdate()
    {
        // UpDownUp, SideToSide

        if (transform.localPosition.x != waypoints[1].x) // means it's x movement
        {
            if (!once && transform.localPosition.x < waypoints[0].x)
            {
                newPosition = new Vector3(waypoints[1].x + 0.01f, waypoints[1].y, waypoints[1].z);
                once = true;
            }
            else if (transform.localPosition.x > waypoints[1].x && once)
            {
                newPosition = new Vector3(waypoints[0].x - 0.01f, waypoints[0].y, waypoints[0].z);
                once = false;
            }
        }
        else if (transform.localPosition.y != waypoints[1].y) // means it's y movement
        {
            if (!once && transform.localPosition.y < waypoints[0].y)
            {
                newPosition = new Vector3(waypoints[1].x, waypoints[1].y + 0.01f, waypoints[1].z);
                once = true;
            }
            else if (transform.localPosition.y > waypoints[1].y && once)
            {
                newPosition = new Vector3(waypoints[0].x, waypoints[0].y - 0.01f, waypoints[0].z);
                once = false;
            }
        }
        else
        {
            Debug.LogWarning("This is a new predicament!");
        }

        transform.position = Vector3.SmoothDamp(transform.localPosition, newPosition, ref velocity, smoothTime, maxSpeed, Time.deltaTime);
    }
}
