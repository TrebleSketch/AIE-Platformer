﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class TheStoryProgression : MonoBehaviour
{
    public static TheStoryProgression instance;

    [SerializeField] GameObject emptyGameObject;

    public List<Sprite> spiritSprites = new List<Sprite>();

    // Player finds themselves in a weird forest
    // Needs to escape the forest
    // Meets this talking pony who help guides through the movement tutorial and game mechanics
    InteractionDialogue dialogue;
    UI_InteractionPanel uiDialogue;
    DialogueOrder[] dialogueOrder;

    PlayerDialogue playerDialogue;
    PlayerMovement playerMovement;
    PlayerTransformation playerTransformation;

    CameraFollow cameraFollow;

    Rect triggerZone;
    // lvl 1 forest
    // lvl 2 underground
    // lvl 3 seaside and escape

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        StartCoroutine(nameof(TheStoryLine_Map1));
    }

    IEnumerator TheStoryLine_Map1()
    {
        // Map 1
        SceneInitialization();

        // Control Lock
        dialogue.shouldMove = false;

        // Disallow player to check for interaction
        playerDialogue.SetCanCheckForInteraction(false);

        // Activated dialogue UI
        uiDialogue.SetDialogueBoxVisibility(true);

        int order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        // This reverses stuff to allow player to interact and move again
        TogglePlayerInteractability();






        // player moves up
        triggerZone = new Rect(new Vector2(6.5f, 0f), new Vector2(1f, 2f));
        yield return new WaitUntil(() => IsPlayerInTriggerZone(triggerZone));

        // player "realises" how to move again, this trigger the movement tutorial
        dialogueOrder = SearchWithDialogueName("Movement Tutorial");

        TogglePlayerInteractability();

        order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        TogglePlayerInteractability();







        // This logic will be for higher jumps
        // HOLD DOWN X for higher jumps
        // FINISH THIS TONIGHT

        // Check if player is in the zone
        triggerZone = new Rect(new Vector2(16.5f, 3.5f), new Vector2(1f, 2f));
        yield return new WaitUntil(() => IsPlayerInTriggerZone(triggerZone));

        // get the dialogue
        dialogueOrder = SearchWithDialogueName("Tall Jump Tutorial");

        TogglePlayerInteractability();

        order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        TogglePlayerInteractability();







        // This logic will be for right side of platforms, add some other tutorial stuff
        // meeting the spirit
        // FINISH THIS TONIGHT

        // Learn about platforms

        // Check if player is in the zone
        triggerZone = new Rect(new Vector2(25f, 3.5f), new Vector2(1f, 2f));
        yield return new WaitUntil(() => IsPlayerInTriggerZone(triggerZone));

        // get the dialogue
        dialogueOrder = SearchWithDialogueName("Platform Tutorial");

        TogglePlayerInteractability();

        order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        TogglePlayerInteractability();






        // Meets the spirit

        // manual animation runs here

        // creation of the GO
        Vector2 spawnPosition = new Vector2(66.5f, -5f);
        GameObject spirit = Instantiate(emptyGameObject, spawnPosition, Quaternion.identity, playerMovement.transform.parent);
        SpriteRenderer spriteRenderer = spirit.AddComponent<SpriteRenderer>();
        SpriteAnimationViaScript spriteAnimation = spirit.AddComponent<SpriteAnimationViaScript>();
        spriteRenderer.sprite = spiritSprites[0];
        spriteRenderer.flipX = true;
        spirit.name = "Spirit";

        // Meets the spirit 

        // Check if player is in the zone 
        triggerZone = new Rect(new Vector2(59.5f, 4f), new Vector2(2f, 8f));
        yield return new WaitUntil(() => IsPlayerInTriggerZone(triggerZone));

        TogglePlayerInteractabilityWithoutDialogueShowing();

        // float to { 66.5, 1.5 }
        // then to { 65, 3.5 }
        // then to { 63.5, 3 }
        Vector3 newPos1 = new Vector3(66.5f, 1.5f, 0);
        Vector3 newPos2 = new Vector3(65, 3.5f, 0);
        Vector3 newPos3 = new Vector3(63.5f, 3, 0);

        spriteAnimation.instance.AddAnimation(newPos1);
        yield return new WaitUntil(() => spriteAnimation.IsAnimationCusNull());
        spriteAnimation.instance.AddAnimation(newPos2);
        yield return new WaitUntil(() => spriteAnimation.IsAnimationCusNull());
        spriteAnimation.instance.AddAnimation(newPos3);
        yield return new WaitUntil(() => spriteAnimation.IsAnimationCusNull());

        print("start convo");

        ToggleDialogueBox();

        // Then start the conversation 
        dialogueOrder = SearchWithDialogueName("The Meet");
        order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method 
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue 
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        // animation of going towards the character before disappearing in a shower of particles once at the player
        spriteAnimation.instance.AddAnimation(playerMovement.transform.position);
        yield return new WaitUntil(() => spriteAnimation.IsAnimationCusNull());

        // activate player particles


        //remove spirit GO
        Destroy(spirit);


        // conversation continues
        dialogueOrder = SearchWithDialogueName("The Meet, Part 2");
        order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method 
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue 
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        TogglePlayerInteractability();







        // Right before the small tunnel, tutorial the player to transform into spirit 1
        triggerZone = new Rect(new Vector2(3.6f, -7f), new Vector2(1f, 2f));
        yield return new WaitUntil(() => IsPlayerInTriggerZone(triggerZone));

        // spirit reminds the player of the new ability, this trigger the transform tutorial
        dialogueOrder = SearchWithDialogueName("Transformation Tutorial");

        // Player learns how to transform
        TogglePlayerInteractability();

        order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method
            if (order > 1)
                yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        // enables transform capability
        playerTransformation.SetIsThePlayerAllowedToTransform(true);

        TogglePlayerInteractability();

        // player transforms and enters tunnel
        yield return new WaitUntil(() => Input.GetButtonDown("Transform"));






        // WaitUntil => Player is now jumping into the chute
        triggerZone = new Rect(new Vector2(-3f, -8f), new Vector2(2f, 0.8f));
        yield return new WaitUntil(() => IsPlayerInTriggerZone(triggerZone));

        // Starts fading the screen black as player falls
        Debug.Log("fallen into chute");

        // Player falls out of camera range and scene changes
        yield return new WaitUntil(IsPlayerOutOfViewOfCamera);

        Debug.LogWarning("End of scene!");

        // Start the next map's function
        //StartCoroutine(nameof(TheStoryLine_Map2));
    }

    IEnumerator TheStoryLine_Map2()
    {
        Debug.LogWarning("Scene change!!!");

        // change to map2 (sceneID = 7)
        MapChange(7);

        Debug.Log("Scene has been changed");

        // Initialisation
        SceneInitialization();

        Debug.LogWarning("There is nothing more right now");

        yield return null;
    }

    void SceneInitialization()
    {
        dialogue = InteractionDialogue.instance;
        uiDialogue = UI_InteractionPanel.instance;
        dialogueOrder = SearchWithDialogueName("Intro");

        playerDialogue = PlayerDialogue.instance;
        playerMovement = PlayerMovement.instance;
        playerTransformation = PlayerTransformation.instance;
        if (playerTransformation == null)
            Debug.LogWarning("PlayerTransformation variable is null");

        cameraFollow = CameraFollow.instance;
    }

    bool IsPlayerOutOfViewOfCamera()
    {
        return playerMovement.transform.position.y < (cameraFollow.transform.position.y - (cameraFollow.ScreenSize.y / 2));
    }
    
    bool IsPlayerInTriggerZone(Rect rectangle)
    {
        return rectangle.Contains(playerMovement.transform.position);
    }

    void TogglePlayerInteractability()
    {
        dialogue.shouldMove = !dialogue.shouldMove;
        playerDialogue.SetCanCheckForInteraction(!playerDialogue.CanCheckForInteraction);
        uiDialogue.SetDialogueBoxVisibility(!uiDialogue.IsDialogueBoxVisible);
    }

    void TogglePlayerInteractabilityWithoutDialogueShowing()
    {
        dialogue.shouldMove = !dialogue.shouldMove;
        playerDialogue.SetCanCheckForInteraction(!playerDialogue.CanCheckForInteraction);
    }

    void ToggleDialogueBox()
    {
        uiDialogue.SetDialogueBoxVisibility(!uiDialogue.IsDialogueBoxVisible);
    }

    DialogueOrder[] SearchWithDialogueName(string searchingName)
    {
        for (int i = 0; i < dialogue.listOfSceneDialogues.Count; i++)
        {
            if (dialogue.listOfSceneDialogues[i].dialogueName != searchingName)
                continue;
            return dialogue.listOfSceneDialogues[i].dialogueOrder;
        }
        Debug.LogError("Dialogue not found...");
        return null;
    }

    static IEnumerator MapChange(int sceneId)
    {
        AsyncOperation unloading = SceneManager.UnloadSceneAsync(SceneData.instance.sceneID);
        yield return new WaitUntil(() => unloading.isDone);
        AsyncOperation nextSceneAsync = SceneManager.LoadSceneAsync(sceneId, LoadSceneMode.Additive);
        yield return new WaitUntil(() => nextSceneAsync.isDone);
    }
}
