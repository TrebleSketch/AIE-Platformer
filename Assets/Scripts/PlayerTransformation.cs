﻿using UnityEngine;
using System.Collections;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class PlayerTransformation : MonoBehaviour
{
    public static PlayerTransformation instance;

    public bool IsAbleToTransform => isAbleToTransform;
    bool isAbleToTransform = false;

    bool isTransforming;
    public bool IsTransforming => isTransforming;

    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Sprite anthroSprite;
    [SerializeField] Sprite feralSprite;
    public bool IsFeral => isFeral;
    bool isFeral = false;

    protected readonly Vector2 anthroSize = new Vector2(1f, 1.3f);
    protected readonly Vector2 feralSize = new Vector2(0.8f, 0.9f);

    [SerializeField] Collider2D playerCollider2D;
    protected readonly Vector2 anthroColliderPosition = Vector2.zero;
    protected readonly Vector2 anthroColliderSize = new Vector2(0.68f, 1f);
    protected readonly Vector2 feralColliderPosition = new Vector2(0, -0.06f);
    protected readonly Vector2 feralColliderSize = new Vector2(0.89f, 0.87f);

    Vector2 sizeDifferenceHalfed;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Start()
    {
        sizeDifferenceHalfed = new Vector2(0, (anthroSize.y - feralSize.y) / 2);
    }

    void Update ()
    {
		if (Input.GetButtonDown("Transform") && !isTransforming && isAbleToTransform)
        {
            isTransforming = true;
            StartCoroutine(TF());
        }
	}

    // :>
    IEnumerator TF()
    {
        isFeral = !isFeral;
        // if false at first, you are now a feral
        // if true at first, you are now back to an anthro

        // stuff to do:
        // Particles?
        // Ridgidbody changes (weight, max jump height)

        playerCollider2D.GetComponent<BoxCollider2D>().size = isFeral ? feralColliderSize : anthroColliderSize;
        playerCollider2D.offset = isFeral ? feralColliderPosition : anthroColliderPosition;

        spriteRenderer.sprite = isFeral ? feralSprite : anthroSprite;

        transform.localScale = isFeral ? feralSize : anthroSize;
        transform.position = new Vector2(transform.position.x,
            isFeral ?
            transform.position.y - sizeDifferenceHalfed.y :
            transform.position.y + sizeDifferenceHalfed.y);

        isTransforming = false;
        yield return null;
    }

    public void SetIsThePlayerAllowedToTransform(bool toggle)
    {
        isAbleToTransform = toggle;
    }
}
