﻿using System;
using UnityEngine;
using System.Collections.Generic;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    // Source: https://www.youtube.com/watch?v=aLpixrPvlB8
    // Thanks Brackeys

    public static CameraFollow instance;

    public List<Transform> targets;

    public Vector3 offset;
    readonly float smoothTime = 0.4f;

    float originalZoom;
    public float minZoom = 1.75f;
    public float maxZoom = 5f;
    public float zoomLimiter = 50f;

    private Vector3 velocity;
    private Camera cam;

    Vector3 currentPosition;

    public Vector2 ScreenSize => screenSize;
    Vector2 screenSize;
    [SerializeField] GameObject leftCollider;
    [SerializeField] GameObject rightCollider;
    //float colDepth = 1.5f;
    bool isAtEdge;
    bool isLeft;
    Vector2 theObject;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        cam = GetComponent<Camera>();
        originalZoom = cam.orthographicSize;
        CameraColliderCheck();
        if (targets[0] == null)
            targets[0] = PlayerMovement.instance.gameObject.transform;
    }

    void CameraColliderCheck()
    {
        // Source: https://forum.unity.com/threads/collision-with-sides-of-screen.228865/#post-1523027
        CollidersSizeUpdate();
        CollidersPositionUpdate();
    }

    void LateUpdate()
    {
        if (targets.Count == 0)
            return;

        Move();
        Zoom();
        if (cam.orthographicSize != originalZoom)
            CollidersSizeUpdate();
        CollidersPositionUpdate();
    }

    void CollidersPositionUpdate()
    {
        rightCollider.transform.localPosition = new Vector3(0 + (screenSize.x / 4) /*+ (colDepth / 2)*/, 0, -offset.z);
        leftCollider.transform.localPosition = new Vector3(0 - (screenSize.x / 4) /*- (colDepth / 2)*/, 0, -offset.z);
    }

    void CollidersSizeUpdate()
    {
        screenSize.x = Vector3.Distance(
            cam.ScreenToWorldPoint(new Vector2(0, 0)),
            cam.ScreenToWorldPoint(new Vector2(Screen.width, 0)));
        screenSize.y = Vector3.Distance(
            cam.ScreenToWorldPoint(new Vector2(0, 0)),
            cam.ScreenToWorldPoint(new Vector2(0, Screen.height)));

        rightCollider.GetComponent<BoxCollider2D>().size = new Vector2(screenSize.x / 2, screenSize.y);
        leftCollider.GetComponent<BoxCollider2D>().size = new Vector2(screenSize.x / 2, screenSize.y);
    }

    void Zoom()
    {
        float newZoom = originalZoom;

        if (PlayerTransformation.instance.IsFeral)
            newZoom = 2.85f;

        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, newZoom, Time.deltaTime);
    }

    //void OldZoom()
    //{
    //    // change this for orthographic camera
    //    float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimiter);
    //    cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);
    //}

    void Move()
    {
        Vector3 newPosition = offset;
        currentPosition = transform.position;

        if (!isAtEdge)
        {
            newPosition += GetCentrePoint();

            if (Mathf.Abs(currentPosition.x - newPosition.x) < 0.1f)
                currentPosition.x = newPosition.x;
        }
        else
        {
            if (isLeft)
            {
                if ((transform.position.x - targets[0].position.x) < -0.1f)
                    isAtEdge = false;
            }
            else
            {
                if ((transform.position.x - targets[0].position.x) > 0.1f)
                    isAtEdge = false;
            }
        }

        transform.position = !isAtEdge ? Vector3.SmoothDamp(currentPosition, newPosition, ref velocity, smoothTime) : Vector3.SmoothDamp(currentPosition, new Vector3(currentPosition.x, targets[0].position.y, offset.z), ref velocity, smoothTime);
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.size.x;
    }

    public void StartedEdgeCollision(bool isLeftFromCollisions, GameObject collidingEdge)
    {
        isAtEdge = true;
        isLeft = isLeftFromCollisions;
        theObject = collidingEdge.transform.position;
        currentPosition.x = Vector2.Distance(new Vector2(theObject.x + ((isLeft ? -collidingEdge.GetComponent<BoxCollider2D>().size.x : collidingEdge.GetComponent<BoxCollider2D>().size.x))/* if left, minus the width of the gameobject itself*/, 0), new Vector2(screenSize.x / 2, 0));
    }

    //public bool CheckIfAlreadyColliding()
    //{
    //    return isAtEdge;
    //}

    //public void StoppedEdgeCollision()
    //{
    //    isAtEdge = false;
    //}

    //Vector3 CalculateOffset()
    //{
    //    // wip: change the offset as player approaches the edge of the map
    //    // this will require a re-write when multiplayer might be considered in the future
    //    if (isAtEdge)
    //    {
    //        //print("derp");
    //        //offset.x = Vector2.Distance(new Vector2(isLeft ? leftCollider.transform.position.x : rightCollider.transform.position.x, 0), new Vector2(targets[0].transform.position.x, 0));
    //        //print("Screensize: " + screenSize.x / 2);
    //        //print(Vector2.Distance(new Vector2(theObject.x, 0), new Vector2(targets[0].transform.position.x, 0)));

    //        offset.x = (Vector2.Distance(new Vector2(isLeft ? theObject.x + (leftCollider.GetComponent<BoxCollider2D>().offset.x / 2) : theObject.x - (rightCollider.GetComponent<BoxCollider2D>().offset.x / 2), 0), new Vector2(targets[0].transform.position.x, 0)) * 2) + (isLeft ? -(screenSize.x / 2) : (screenSize.x / 2));
    //        print(offset.x);
    //    }
    //    else if (Math.Abs(offset.x) > 0.01f && !isAtEdge)
    //        offset.x = 0;
    //    return offset;
    //}

    Vector3 GetCentrePoint()
    {
        if (targets.Count == 1)
        {
            return targets[0].position;
        }

        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.center;
    }
}
