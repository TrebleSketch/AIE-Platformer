﻿using UnityEngine;
using UnityEngine.UI;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class UI_InteractionPanel : MonoBehaviour
{
    public static UI_InteractionPanel instance;
    //InteractionDialogue dialogueSource;

    // Debug stuff
    [SerializeField] bool shouldDialogueBoxShow;

    // Text where dialogue will be written
    [SerializeField] GameObject dialogueBoxGO;
    public Text DialogueText => dialogueText;
    [SerializeField] Text dialogueText;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        //dialogueSource = InteractionDialogue.instance;
        InternalSetDialogueBoxVisibility();
    }

    void InternalSetDialogueBoxVisibility() => dialogueBoxGO.SetActive(shouldDialogueBoxShow);

    public bool IsDialogueBoxVisible => dialogueBoxGO.activeInHierarchy;

    public void SetDialogueBoxVisibility(bool shouldBeVisible)
    {
        shouldDialogueBoxShow = shouldBeVisible;
        dialogueBoxGO.SetActive(shouldDialogueBoxShow);
    }

    public void InputTextInBox(string dialogue)
    {
        dialogueText.text = dialogue;
    }
}
