﻿using UnityEngine;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class PlayerMovement : PhysicsObject
{
    public static PlayerMovement instance;

    [SerializeField] SpriteRenderer spriteRenderer;
    Vector2 spawnPoint => SceneData.instance.SpawnPoint;

    readonly float maxSpeed = 4.5f;
    readonly float jumpTakeOffSpeed = 8f;

    protected override void ComputerVelocity()
    {
        Vector2 move = Vector2.zero;                

        //animator.SetBool("grounded", grounded);
        //animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        if (InteractionDialogue.instance != null)
            if (InteractionDialogue.instance.shouldMove)
            {
                move.x = Input.GetAxis("Horizontal");

                if (Input.GetButtonDown("Jump") && grounded)
                {
                    velocity.y = jumpTakeOffSpeed;
                }
                else if (Input.GetButtonUp("Jump"))
                {
                    if (velocity.y > 0)
                        velocity.y = velocity.y * 0.5f;
                }

                if (Mathf.Abs(move.x) > 0.005f)
                    spriteRenderer.flipX = (!(move.x > 0.005f));
            }

        if (Input.GetKeyDown(KeyCode.F12))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

        targetVelocity = move * maxSpeed;
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    new void Start()
    {
        base.Start();

        transform.position = spawnPoint;
    }

    new void FixedUpdate()
    {
        base.FixedUpdate();

        if (transform.position.y < -50f)
            transform.position = spawnPoint;
    }

    public void IsGrounded(PlayerParticles theclass)
    {
        IsGroundedEvent += theclass.FootParticles;
        IsMovingEvent += theclass.MovingParticles;
    }

    //private void OnCollisionStay2D(Collision2D other)
    //{
    //    if (Math.Abs(other.transform.rotation.z) < 0.01f)
    //    {
    //        if (transform.rotation != other.transform.rotation)
    //            transform.rotation = other.transform.rotation;
    //        return;
    //    }
    //    transform.rotation = other.transform.rotation;
    //}
}
