﻿using UnityEngine;
using UnityEngine.SceneManagement;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class SceneData : MonoBehaviour
{
    public static SceneData instance;

    public int sceneID => GetSceneID();
    static int GetSceneID() { return SceneManager.GetActiveScene().buildIndex; }

    public string sceneName = "Default Scene";

    // Base class for each scene

    public Vector2 SpawnPoint;

    // Shows the zoom depending on how far along the map it is

    void Awake()
    {
        if (TheStoryProgression.instance == null)
            SceneManager.LoadSceneAsync(5, LoadSceneMode.Additive);

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
}
