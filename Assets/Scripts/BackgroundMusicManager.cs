﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;

// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

[RequireComponent(typeof(AudioSource))]
public class BackgroundMusicManager : MonoBehaviour
{
    public enum fileType
    {
        mp3,
        ogg
    }

    [SerializeField] bool shouldPlayFirst;
    [SerializeField] bool shouldLoop;
    [SerializeField] fileType Type;

    AudioSource audioPlayer;

    public List<AudioClip> audioPlaylist = new List<AudioClip>();
    int audioPlaylistPosition;

    void Awake()
    {
        audioPlayer = GetComponent<AudioSource>();

        if (SceneManager.GetActiveScene().name != "test2_audio")
        {
            return;
        }
        // may add songs procedually

        DirectoryInfo directoryInfo = new DirectoryInfo(Application.streamingAssetsPath);
        //print("Streaming Assets Path: " + Application.streamingAssetsPath);
        FileInfo[] allFiles = directoryInfo.GetFiles("*.*", SearchOption.AllDirectories);

        foreach (FileInfo file in allFiles)
        {
            //print(file.FullName);
            if (file.FullName.Contains("ogg"))
                StartCoroutine(nameof(LoadBackgroundMusic), file);
        }
    }

    IEnumerator LoadBackgroundMusic(FileInfo musicFile)
    {
        if (musicFile.Name.Contains("meta")) yield break;
        //print("test");
        string musicFilePath = musicFile.FullName.Replace("\\", "/");
        string url = $"file://{musicFilePath}";
        //print(url);
        WWW www = new WWW(url);
        yield return www;
        //print(www.url);
        audioPlaylist.Add(www.GetAudioClip(false, false));
    }





    void Start()
    {
        if (!shouldPlayFirst) return;

        // Can only play one song at a time, no modular song selection yet
        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 6:
                // Map 1 song selection
                InternalPickSpecificSong("Ascending the Vale");
                break;
            default:
                Debug.LogWarning("No audio had been assigned for this scene. Add it if you want audio in this scene.");
                break;
        }
        
        audioPlayer.loop = shouldLoop;
        ToggleStartSong();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
            InternalSongSelection(true);
        else if (Input.GetKeyDown(KeyCode.H))
            InternalSongSelection(false);

        if (Input.GetKeyDown(KeyCode.U) && !audioPlayer.isPlaying)
        {
            audioPlayer.clip = audioPlaylist[0];
            ToggleStartSong();
        }
    }

    // mp3s can't stream, so only oggs can stream!


    public void ToggleStartSong()
    {
        if (/*Input.GetKeyDown(KeyCode.O) && */!audioPlayer.isPlaying)
            audioPlayer.Play();
        else
            Debug.LogWarning("Audio Player is already playing something");
    }

    public void TogglePauseSong()
    {
        //if (Input.GetKeyDown(KeyCode.U))

        if (audioPlayer.isPlaying)
            audioPlayer.Pause();
        else
            audioPlayer.UnPause();
    }

    public void ToggleStopSong()
    {
        //if (Input.GetKeyDown(KeyCode.I))
        if (!audioPlayer.isPlaying)
            Debug.LogWarning("Audio Player isn't playing anything");
        audioPlayer.Stop();
    }

    public void SongSelection(bool shouldSelectNextSong)
    {
        InternalSongSelection(shouldSelectNextSong);
    }

    void InternalSongSelection(bool selectNextSong)
    {
        if (audioPlaylist.Count < 2)
            return;

        if (selectNextSong)
        {
            if (audioPlaylistPosition + 1 < audioPlaylist.Count)
                audioPlaylistPosition++;
            else if (audioPlaylistPosition + 1 > audioPlaylist.Count - 1)
                audioPlaylistPosition = 0;
        }
        else
        {
            if (audioPlaylistPosition - 1 > -1)
                audioPlaylistPosition--;
            else if (audioPlaylistPosition - 1 == -1)
                audioPlaylistPosition = audioPlaylist.Count - 1;
        }
        if (audioPlayer.isPlaying)
        {
            audioPlayer.clip = audioPlaylist[audioPlaylistPosition];
            audioPlayer.Play();
        }
        else
            audioPlayer.clip = audioPlaylist[audioPlaylistPosition];

    }

    void InternalPickSpecificSong(string songName)
    {
        for (var audioIndex = 0; audioIndex < audioPlaylist.Count; audioIndex++)
        {
            if (!audioPlaylist[audioIndex].name.Contains(songName)) continue;
            if (!audioPlaylist[audioIndex].name.Contains(Type.ToString())) continue;
            audioPlaylistPosition = audioIndex;
            audioPlayer.clip = audioPlaylist[audioPlaylistPosition];
        }
    }
}
