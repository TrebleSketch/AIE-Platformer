﻿using UnityEngine;
using UnityEditor;

public class MapToggleBoxCollider : ScriptableObject
{
    [MenuItem("Debug Tools/Disable All Empty Sprite's Colliders")]
    static void DoIt()
    {
        foreach (SpriteRenderer sprites in FindObjectsOfType<SpriteRenderer>())
        {
            if (sprites.sprite != null && sprites.GetComponent<BoxCollider2D>().isActiveAndEnabled)
                continue;
            else if (sprites.sprite == null)
                sprites.GetComponent<BoxCollider2D>().enabled = false;
            else if (sprites.sprite != null && !sprites.GetComponent<BoxCollider2D>().isActiveAndEnabled)
                sprites.GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}