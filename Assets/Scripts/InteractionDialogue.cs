﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

[Serializable]
public class Dialogue
{
    // Each gameObject in the scene will get their own dialogue order

    // GameObject that this dialogue applies to
    public GameObject go;
    // Dialogue name
    public string dialogueName;
    // If true, this dialogue can only be activated by script
    public bool scriptActivationOnly = false;
    // if true, linear dialogue. if false, non-linear dialogue
    public bool isNonLinearDialogue = false;
    // The array filled with the dialogue
    public DialogueOrder[] dialogueOrder;
}

[Serializable]
public class DialogueOrder
{
    // Which dialogue in this array should the dialogue go next
    public int nextDialogue;
    // Dialogue text
    public string dialogue;
    public Image characterReaction;

    // Add location of the dialogue box (top/middle/bottom)
    // Add selection of "nextDialogue" capabilities
    // Add animation effects?
    // Add a check if it is the last one?
}

public class InteractionDialogue : MonoBehaviour
{
    public static InteractionDialogue instance;

    public bool shouldMove = true;

    public List<Dialogue> listOfSceneDialogues;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);   
    }
}
