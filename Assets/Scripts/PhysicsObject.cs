﻿using UnityEngine;
using System.Collections.Generic;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class PhysicsObject : MonoBehaviour
{
    // event is called when player is grounded
    public delegate void PlayerParticlesIsGrounded();
    public static event PlayerParticlesIsGrounded IsGroundedEvent;
    public delegate void PlayerParticlesIsMoving();
    public static event PlayerParticlesIsMoving IsMovingEvent;

    public float minGroundNormalY = 0.65f;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;

    protected Vector2 targetVelocity;
    protected bool grounded;
    bool groundedCheck;
    protected Vector2 groundNormal;
    protected Rigidbody2D rb;
    protected Vector2 velocity;
    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    protected const float minMoveDistance = 0.001f;
    protected const float shellRadius = 0.01f;

    float distance;
    Vector2 move;

    protected void OnEnable()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    protected void Start()
    {
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
    }

    protected void Update ()
    {
        targetVelocity = Vector2.zero;
        ComputerVelocity();
    }

    protected virtual void ComputerVelocity() { }

    protected void FixedUpdate()
    {
        if (velocity.y < 0)
            velocity += (fallMultiplier - 1) * Physics2D.gravity * Time.deltaTime;
        else if (velocity.y > 0 && !Input.GetButton("Jump"))
            velocity += (lowJumpMultiplier - 1) * Physics2D.gravity * Time.deltaTime;
        else
            velocity += (fallMultiplier - 1) * Physics2D.gravity * Time.deltaTime;
        velocity.x = targetVelocity.x;

        grounded = false;

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

        move = moveAlongGround * deltaPosition.x;

        Movement(move, false);

        move = Vector2.up * deltaPosition.y;

        Movement(move, true);
    }

    void Movement(Vector2 move, bool yMovement)
    {
        distance = move.magnitude;

        if (distance > minMoveDistance)
        {
            int count = rb.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++)
            {
                Vector2 currentNormal = hitBufferList[i].normal;
                if (currentNormal.y > minGroundNormalY)
                {
                    grounded = true;
                    
                    if (yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                        if (groundedCheck != grounded && !Input.GetButton("Jump"))
                        {
                            if (move.x != 0 && !Input.GetButton("Horizontal"))
                                IsGroundedEvent?.Invoke();
                            else if (Input.GetButton("Horizontal"))
                                IsMovingEvent?.Invoke();
                            groundedCheck = true;
                        }
                    }
                }

                float projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0)
                {
                    velocity = velocity - projection * currentNormal;
                }

                float modifiedDistance = hitBufferList[i].distance - shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }

            if (hitBufferList.Count == 0)
                groundedCheck = false;
        }

        rb.position = ReturnMovement();
    }

    public Vector2 ReturnMovement()
    {
        return rb.position + move.normalized * distance;
    }
}
