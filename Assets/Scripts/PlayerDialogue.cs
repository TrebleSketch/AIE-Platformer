﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class PlayerDialogue : MonoBehaviour
{
    public static PlayerDialogue instance;

    // This interacts with the InteractionDialogue
    // And UI_InteractionPanel when selection is needed
    bool canCheckForInteraction = true;
    public bool CanCheckForInteraction => canCheckForInteraction;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Update()
    {
        if (Input.GetButtonDown("Interact") && canCheckForInteraction)
            StartCoroutine(nameof(CheckIfObjectIsInList));
            //Invoke(nameof(CheckIfObjectIsInList), 0);
    }

    IEnumerator CheckIfObjectIsInList()
    {
        canCheckForInteraction = false;
        Debug.Log("Interact button clicked");

        //// Get all of the objects just around the player
        //GameObject objectToInteractWith = null;
        InteractionDialogue dialogue = InteractionDialogue.instance;
        UI_InteractionPanel uiDialogue = UI_InteractionPanel.instance;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 1f);
        Dictionary<float, GameObject> interactableGameObjectsWithinRange = new Dictionary<float, GameObject>();

        // <key, value>

        DialogueOrder[] dialogueOrder = null;

        //print(colliders.Length);

        for (int i = 0; i < colliders.Length; i++)
        {
            //print("collider name: " + colliders[i].name);
            if (colliders[i].gameObject.layer == LayerMask.NameToLayer("Collidable Surface"))
                continue;
            foreach (Dialogue dialogueList in dialogue.listOfSceneDialogues)
            {
                if (dialogueList.go != colliders[i].gameObject) continue;

                if (dialogueList.scriptActivationOnly) continue;

                // Check if they are interactable by checking if the gameobject is in "InteractionDialogue"
                interactableGameObjectsWithinRange.Add(Vector3.Distance(transform.position, colliders[i].gameObject.transform.position), colliders[i].gameObject);
                // add the dialogue locally to be cycled through
                dialogueOrder = dialogueList.dialogueOrder;
                Debug.Log("Detected OwO", colliders[i].gameObject);
            }
        }

        if (interactableGameObjectsWithinRange.Count == 0) // If object isn't in the list, return
        {
            Debug.LogWarning("There is nothing in range to interact with.");
            canCheckForInteraction = true;
            yield break;
        }

        if (dialogueOrder == null)
        {
            Debug.LogWarning("There is no dialogue list in this gameobject.");
            canCheckForInteraction = true;
            yield break;
        }

        // confirmed that there is something to interact with, lock player's movement
        InteractionDialogue.instance.shouldMove = false;

        //print("Distance between player and interactable object: " + interactableGameObjectsWithinRange.Keys.Min());
        GameObject minDistGameObject = interactableGameObjectsWithinRange[interactableGameObjectsWithinRange.Keys.Min()];

        Debug.Log("It's a " + minDistGameObject.name + "!!!", minDistGameObject);
        //return;

        if (uiDialogue.IsDialogueBoxVisible)
            Debug.LogWarning("Dialogue box is already enabled, better check that out.", uiDialogue);

        // Then activate the dialogue UI
        uiDialogue.SetDialogueBoxVisibility(true);

        // Cycle through the dialogue
        //DialogueLoop(dialogueOrder, uiDialogue);
        int order = 0;
        while (order < dialogueOrder.Length)
        {
            uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
            order++;
            // Make sure the button is up, bodge method
            yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
            // Check for clicks to continue to next dialogue
            yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
        }

        // In the future, move the dialogue by what is in the int "nextDialogue"

        Debug.Log("Reached the end of the dialogue, click once more to end the dialogue. Thanks.", this);

        // Disable the dialogue box
        uiDialogue.SetDialogueBoxVisibility(false);

        // Can check for interaction again
        canCheckForInteraction = true;

        // Can move again
        InteractionDialogue.instance.shouldMove = true;
    }

    public void SetCanCheckForInteraction(bool isInteractable)
    {
        canCheckForInteraction = isInteractable;
    }

    //IEnumerator DialogueLoop(DialogueOrder[] dialogueOrder, UI_InteractionPanel uiDialogue)
    //{
    //    // Cycle through the dialogue
    //    int order = 0;
    //    while (order < dialogueOrder.Length)
    //    {
    //        uiDialogue.InputTextInBox(dialogueOrder[order].dialogue);
    //        order++;
    //        // Make sure the button is up, bodge method
    //        yield return new WaitUntil(() => Input.GetButtonUp("Interact"));
    //        // Check for clicks to continue to next dialogue
    //        yield return new WaitUntil(() => Input.GetButtonDown("Interact"));
    //    }
    //}
}
