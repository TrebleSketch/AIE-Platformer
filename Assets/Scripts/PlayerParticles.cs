﻿using UnityEngine;

public class PlayerParticles : MonoBehaviour
{
    [SerializeField] ParticleSystem footEmitter;
    [SerializeField] ParticleSystem runningEmitter;

    void Start()
    {
        GetComponent<PlayerMovement>().IsGrounded(this);
    }

    public void FootParticles()
    {
        footEmitter.Play();
    }

    public void MovingParticles()
    {
        runningEmitter.Play();
    }
}
