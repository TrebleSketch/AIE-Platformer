﻿using UnityEngine;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class SpriteAnimationViaScript : MonoBehaviour
{
    public SpriteAnimationViaScript instance;

    delegate void SpriteAnimation();
    static event SpriteAnimation AnimationUpdates;

    Vector3 animationVelocity;
    Vector3 animationLocation;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Update()
    {
        AnimationUpdates?.Invoke();
    }

    public bool IsAnimationCusNull()
    {
        return AnimationUpdates == null;
    }

    public void AddAnimation(Vector3 newPos)
    {
        print("new animation!");
        animationLocation = newPos;
        AnimationUpdates += AddSpriteAnimation;
        print("added animation");
    }

    void AddSpriteAnimation(/*Vector3 newPos*/)
    {
        if (CheckIfPositionIsClose())
        {
            print("animation finished, removing self");
            AnimationUpdates -= AddSpriteAnimation;
        }
        transform.position = Vector3.SmoothDamp(transform.position, animationLocation, ref animationVelocity, 0.75f, 1.5f);
    }

    bool CheckIfPositionIsClose() => Mathf.Abs(transform.position.x - animationLocation.x) < 0.1f &&
                                     Mathf.Abs(transform.position.y - animationLocation.y) < 0.1f;
}
