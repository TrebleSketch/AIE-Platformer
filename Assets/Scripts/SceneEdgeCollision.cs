﻿using UnityEngine;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class SceneEdgeCollision : MonoBehaviour
{
    // This script is used for detecting the edge of the map

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != 20)
            return;
        if (collision.gameObject.transform.parent == transform.parent)
            return;

        print(gameObject.name + " triggered with: " + collision.gameObject.name + "@" + Time.realtimeSinceStartup);
        // Source https://forum.unity.com/threads/unity2d-collision-get-left-right-side-of-collision.282374/#post-1863585
        CameraFollow.instance.StartedEdgeCollision(!(transform.InverseTransformPoint(collision.transform.position).x > 0), collision.gameObject);
    }
}
