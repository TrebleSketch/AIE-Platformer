﻿using UnityEngine;
using UnityEngine.UI;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class UI_PauseMenu : MonoBehaviour
{
    public static UI_PauseMenu instance;

    Animator animator;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            animator.SetBool("isOpen", !animator.GetBool("isOpen"));
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            if (animator.GetBool("isOpen"))
            {
                Debug.Log("End game...");
                Application.Quit();
            }
        }
    }
}
