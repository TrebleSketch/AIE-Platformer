﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;

// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

//public class Background
//{
//    public SpriteRenderer[] sprites;
//}

public class ScrollingBackground : MonoBehaviour
{
    // some rewritten logic from https://www.youtube.com/watch?v=QkisHNmcK7Y


    // when player gets near to one of the edges of the bound, it will create another sprite
    // "duplicates" this one except the script and set it "ahead"
    // Then once it moves too far away, the background will be deleted.
    // Can't delete this slide. So will need some code workaround

    // LOOK UP "PARALAXING IN UNITY"

    enum BackgroundType
    {
        Foreground,
        Background,
        BackgroundColour
    }

    [SerializeField] bool shouldDebug;

    public List<Transform> scrollingBackgrounds;
    [SerializeField] Vector2 fixedPointInSpace;
    float backgroundLerpRate = 0.4f;

    const float distanceBetweenBackgrounds = 21.875f;
    const float backgroundSize = 43.75f;

    Dictionary<BackgroundType, List<int>> DictionaryOfListOfBackgroundTypes = new Dictionary<BackgroundType, List<int>>();
    Dictionary<Transform, int> BackgroundLayers = new Dictionary<Transform, int>();

    // foreground = 0.85
    // background = 0.6
    // background colour = 1 (since it should be static
    // up/down movement = 0.15 (very tiny as the clearance is very tiny too)

    void Awake()
    {
        fixedPointInSpace = transform.position;
    }

    void Start()
    {
        // Source: https://stackoverflow.com/a/856165
        for (int i = 0; i < Enum.GetNames(typeof(BackgroundType)).Length; i++)
        {
            DictionaryOfListOfBackgroundTypes.Add((BackgroundType)i, new List<int>());
        }

        foreach (Transform slide in transform.GetComponentsInChildren<Transform>())
        {
            if (slide == transform)
                continue;
            scrollingBackgrounds.Add(slide);

            int intToAdd;

            if (slide.name.Contains("Foreground"))
            {
                intToAdd = slide.GetSiblingIndex() + 1;

                BackgroundLayers.Add(slide, intToAdd);

                // Source: https://stackoverflow.com/a/12169455
                List<int> localList = DictionaryOfListOfBackgroundTypes[BackgroundType.Foreground];
                localList.Add(intToAdd);
                // preferablly add into the array here
            }
            else if (slide.name.Contains("Background") && !slide.gameObject.name.Contains("Colour"))
            {
                intToAdd = slide.GetSiblingIndex() + 1 - BackgroundLayers.Max(x => x.Value);

                //Debug.Log(slide.GetSiblingIndex() - backgroundLayers.Max(x => x.Value));
                // Source: https://www.codeproject.com/Answers/619720/get-the-key-of-the-highest-value-of-a-Dictionary-i
                BackgroundLayers.Add(slide, intToAdd);

                List<int> localList = DictionaryOfListOfBackgroundTypes[BackgroundType.Background];
                localList.Add(intToAdd);
            }
            else if (slide.name.Contains("Background Colour"))
            {
                intToAdd = slide.GetSiblingIndex() + 1 - BackgroundLayers.Count;

                BackgroundLayers.Add(slide, intToAdd);

                List<int> localList = DictionaryOfListOfBackgroundTypes[BackgroundType.BackgroundColour];
                localList.Add(intToAdd);
            }
        }

        if (!shouldDebug)
        {
            ScrollLeft(BackgroundType.Foreground);
            return;
        }

        // Source: http://www.tutorialsteacher.com/csharp/csharp-dictionary
        for (int i = 0; i < BackgroundLayers.Count; i++)
        {
            Debug.Log(string.Format("Key: {0}  |  Value: {1}",
                                                    BackgroundLayers.Keys.ElementAt(i),
                                                    BackgroundLayers[BackgroundLayers.Keys.ElementAt(i)]));
        }

        for (int i = 0; i < DictionaryOfListOfBackgroundTypes.Count; i++)
        {
            string listString = "";
            for (int s = 0; s < DictionaryOfListOfBackgroundTypes[DictionaryOfListOfBackgroundTypes.Keys.ElementAt(i)].Count; s++)
            {
                listString += DictionaryOfListOfBackgroundTypes[DictionaryOfListOfBackgroundTypes.Keys.ElementAt(i)][s] + " ";
            }
            Debug.Log(string.Format("Key: {0}  |  Value: {1}",
                                                    DictionaryOfListOfBackgroundTypes.Keys.ElementAt(i),
                                                    listString));
        }
    }

    void Update()
    {
        // Keep this in Update or LateUpdate?
        foreach (Transform slide in scrollingBackgrounds)
        {
            if (slide.gameObject.name.Contains("Foreground"))
                slide.position = Vector3.right * ((PlayerMovement.instance.ReturnMovement().x * 0.7f) + CheckForSide(slide.gameObject.name));
            else if (slide.gameObject.name.Contains("Background") && !slide.gameObject.name.Contains("Colour"))
                slide.position = Vector3.right * ((PlayerMovement.instance.ReturnMovement().x * 0.6f) + CheckForSide(slide.gameObject.name));
            else if (slide.gameObject.name == "Background Colour")
                slide.position = Vector3.Lerp(slide.position, (Vector2)CameraFollow.instance.transform.position - fixedPointInSpace, 0.6f);

            if (slide.gameObject.name != "Background Colour")
                slide.position = Vector3.Lerp(slide.position, slide.position + (Vector3.up * CameraFollow.instance.transform.position.y * 0.95f), backgroundLerpRate);
        }

        // Continue here - https://youtu.be/QkisHNmcK7Y?t=15m13s

        //foreach (KeyValuePair<BackgroundType, List<int>> backgroundType in DictionaryOfListOfBackgroundTypes)
        //{
            
        //}
    }
    
    float CheckForSide(string objectName)
    {
        if (objectName.Contains("Middle"))
            return 0;
        return objectName.Contains("Left") ? -distanceBetweenBackgrounds : distanceBetweenBackgrounds;
    }

    void ScrollLeft(BackgroundType bg)
    {
        // Check 11:37 in the video linked above

        // "leftIndex" = 0
        // "rightIndex" = the highest item in the list ~list.Count

        // the "lastRight" will equals to the right most background in 'BackgroundType'
        int lastRight = DictionaryOfListOfBackgroundTypes[bg].Count;
        print("lastRight --- " + lastRight);

        string bgType = AddSpacesToSentence(bg.ToString(), true);
        print("bgType --- " + bgType);

        // The position of the "lastRight" will be moved to the left of the most left background
        // the postion of the right most index will equals to "Vector3.right" times by (position of the left most index's position x minus the "backgroundSize")
        foreach (KeyValuePair<Transform, int> background in BackgroundLayers)
        {
            print("bgName --- " + background.Key.name);
            if (!background.Key.name.Contains(bgType))
            {
                //Debug.LogWarning("AHHHhhhhhh, i BROKE", this);
                return;
            }

            for (int i = 0; i < Enum.GetNames(typeof(BackgroundType)).Length; i++)
            {
                if ((BackgroundType)i != bg)
                    continue;

                print("==========");
                print("Background type --- " + ((BackgroundType)i));
                print("does background Key contain bgType?   " + background.Key.name.Contains(bgType));
                print(background.Value == lastRight
                    ? "Is background Value the same as the lastRight?   true"
                    : "Is background Value the same as the lastRight?   false");
                if ((BackgroundType)i == bg && background.Key.name.Contains(bgType) && background.Value == lastRight)
                {
                    // Now I must move the most right to become the most left
                    print("this should mean the most right is moving to the most left");
                    print("the background that should be moving's position: " + background.Key.position);
                }
            }
            print("==============================");
        }
        
        // the "leftIndex" will then be the "rightIndex"
        // the "rightIndex" then be incremented down by one
        // if the "rightIndex" is below zero, "rightIndex" will be equals to the highest item in the list ~list.Count
        foreach (KeyValuePair<BackgroundType, List<int>> list in DictionaryOfListOfBackgroundTypes)
        {
            // this will change the list of ints around            

            if (list.Key != bg)
            {
                continue;
            }

            List<int> localList = list.Value;

        }
    }

    void ScrollRight()
    {
        // Check 11:37 in the video linked above

        // "leftIndex" = 0
        // "rightIndex" = the highest item in the list ~list.Count

        // the "lastLeft" will equals to the left most background in 'BackgroundType'
        // The position of the "lastLeft" will be moved to the right of the most right background
        // the postion of the left most index will equals to "Vector3.right" times by (position of the right most index's position x plus the "backgroundSize")
        // the "rightIndex" will then be the "leftIndex"
        // the "leftIndex" then be incremented up by one
        // if the "leftIndex" is equals to "layers.Length", meaning it has reached the maximum in the index, "leftIndex" will be equals to 0
    }

    // Source: https://stackoverflow.com/a/272929/9578763
    string AddSpacesToSentence(string text, bool preserveAcronyms)
    {
        if (string.IsNullOrWhiteSpace(text))
            return string.Empty;
        StringBuilder newText = new StringBuilder(text.Length * 2);
        newText.Append(text[0]);
        for (int i = 1; i < text.Length; i++)
        {
            if (char.IsUpper(text[i]))
                if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                    (preserveAcronyms && char.IsUpper(text[i - 1]) && 
                     i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                    newText.Append(' ');
            newText.Append(text[i]);
        }
        return newText.ToString();
    }
}
