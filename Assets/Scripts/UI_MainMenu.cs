﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

public class UI_MainMenu : MonoBehaviour
{
    [SerializeField] GameObject menuButtons;

    // Mainmenu, the four option pop forward depending on what is selected
    [SerializeField] List<Text> menuSelections;
    //[SerializeField] List<Text> 
    [SerializeField] int menuItemSelected;

    [SerializeField] Text loadingText;
    int dots = 1;

    const int originalFontSize = 36;
    const int upSizedFont = 40;

    bool shouldLockInput;

    void Awake()
    {
        if (loadingText != null)
            loadingText.text = "";

        for (int i = 0; i < menuButtons.transform.childCount; i++)
        {
            if (!menuButtons.transform.GetChild(i).gameObject.activeInHierarchy) continue;
            menuSelections.Add(menuButtons.transform.GetChild(i).GetComponent<Text>());
        }
        menuSelections[menuItemSelected].fontSize = upSizedFont;
        menuSelections[menuItemSelected].GetComponent<Outline>().enabled = true;
    }
    
    void Update()
    {
        if (shouldLockInput)
            return;

        if (Input.GetButtonDown("Vertical"))
        {
            MenuSelection();
        }
        if (Input.GetButtonDown("Interact"))
        {
            StartCoroutine(MenuSelect());
        }
    }

    void MenuSelection()
    {
        menuSelections[menuItemSelected].fontSize = originalFontSize;
        menuSelections[menuItemSelected].GetComponent<Outline>().enabled = false;

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (menuItemSelected + 1 < menuSelections.Count)
            {
                menuItemSelected++;
                menuSelections[menuItemSelected].fontSize = upSizedFont;
            }
            else if (menuItemSelected + 1 > menuSelections.Count - 1)
            {
                menuItemSelected = 0;
                menuSelections[menuItemSelected].fontSize = upSizedFont;
            }
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (menuItemSelected - 1 > -1)
            {
                menuItemSelected--;
                menuSelections[menuItemSelected].fontSize = upSizedFont;
            }
            else if (menuItemSelected - 1 == -1)
            {
                menuItemSelected = menuSelections.Count - 1;
                menuSelections[menuItemSelected].fontSize = upSizedFont;
            }
        }

        menuSelections[menuItemSelected].GetComponent<Outline>().enabled = true;
    }

    IEnumerator MenuSelect()
    {
        shouldLockInput = true;
        Text selectionText = menuSelections[menuItemSelected];
        switch (selectionText.text)
        {
            case "Start Game":
                if (loadingText != null)
                    InvokeRepeating(nameof(ShortLoadingTextAnimation), 0, 0.85f);
                AsyncOperation startGame = SceneManager.LoadSceneAsync(6, LoadSceneMode.Single);
                yield return new WaitUntil(() => startGame.isDone);
                break;
            case "Basics Mechanics Demo":
                AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
                yield return new WaitUntil(() => sceneLoad.isDone);
                break;
            case "Music Demo":
                AsyncOperation sceneLoad2 = SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
                yield return new WaitUntil(() => sceneLoad2.isDone);
                break;
            case "Scrolling Demo":
                AsyncOperation sceneLoad3 = SceneManager.LoadSceneAsync(3, LoadSceneMode.Single);
                yield return new WaitUntil(() => sceneLoad3.isDone);
                break;
            case "Interaction Demo":
                AsyncOperation sceneLoad4 = SceneManager.LoadSceneAsync(4, LoadSceneMode.Single);
                yield return new WaitUntil(() => sceneLoad4.isDone);
                break;
            case "Continue":
                // Nothing yet
                break;
            case "Settings":
                // Nothing yet
                break;
            case "Exit":
                ExitGame();
                break;
            default:
                Debug.LogWarning("No such button has been programmed!");
                break;
        }

        shouldLockInput = false;
        yield return null;
    }

    public static void ExitGame()
    {
        Debug.Log("Closing game...");
        Application.Quit();
    }

    void ShortLoadingTextAnimation()
    {
        switch (dots)
        {
            case 1:
                loadingText.text = "Loading.";
                break;
            case 2:
                loadingText.text = "Loading..";
                break;
            case 3:
                loadingText.text = "Loading...";
                break;
            case 4:
                loadingText.text = "Loading....";
                dots = 0;
                break;
            default:
                Debug.LogWarning(string.Format("Heck, there shouldn't have been this number of dots {0}", dots));
                break;
        }

        dots++;
    }
}
