﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;
// ReSharper disable ArrangeTypeMemberModifiers
// ReSharper disable SuggestVarOrType_SimpleTypes
// ReSharper disable InconsistentNaming
// ReSharper disable SuggestVarOrType_BuiltInTypes
// ReSharper disable CheckNamespacev

[RequireComponent(typeof(Text))]
public class UI_GameVersion : MonoBehaviour
{
    public bool isVersionOnly;

    StreamReader fileReader;
    string fileLocation;

    void Awake()
    {
        try
        {
            if (Application.isEditor)
                fileLocation = Application.dataPath + "/game version.txt";
            else
                fileLocation = Directory.GetParent(Application.dataPath).FullName + "/game version.txt";

            string checkString = fileLocation.Substring(0, 1);

            if (checkString == "/")
            {
                fileLocation = "http://localhost/AIE-Platformer/game-version.txt";
                Debug.Log("File location is showing as a website. WebGL, ayyyyyy");
            }
        }
        catch (FileNotFoundException)
        {
            Debug.LogWarning("Game Version file not found", this);
            throw;
        }
		
		fileReader = new StreamReader(fileLocation);
		
        if (isVersionOnly)
            GetComponent<Text>().text = "v." + fileReader.ReadLine();
        else
            GetComponent<Text>().text = "AIE-Platformer v" + fileReader.ReadLine();
        fileReader.Close();
    }
}
